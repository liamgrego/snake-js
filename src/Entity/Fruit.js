import Base from "./Base";

export default class Fruit extends Base {
    draw(context, gameState) {
        let transform = this.getTransformedPosition(gameState);
        context.fillRect(transform.x, transform.y, 1 * gameState.scale.x, 1 * gameState.scale.y);
    }
}