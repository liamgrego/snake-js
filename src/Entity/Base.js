import Vector from "../Vector";

export default class Base {
    constructor(x, y, xSpeed, ySpeed) {
        this.position = new Vector(x, y);
        this.speed = new Vector(xSpeed, ySpeed);
    }

    update(gameState) {
        this.position.add(this.speed);
    }

    draw(context, gameState) {}

    getTransformedPosition(gameState) {
        return Vector.add(gameState.position, this.position).multiply(gameState.scale);
    }
}