import Base from "./Base";
import Vector from "../Vector";

export default class Snake extends Base {
    currentDirection;
    directionSpeeds = {
        left:   new Vector(-1, 0),
        up:     new Vector(0, -1),
        right:  new Vector(1, 0),
        down:   new Vector(0, 1)
    };

    constructor(x, y) {
        super(x, y);

        this.dead = false;
        this.length = 5;
        this.snakePositions = [];

        this.setDirection('right');

        this.setEventHandlers(this);
    }

    setDirection(direction) {
        this.speed = this.directionSpeeds[direction];
        this.currentDirection = direction;
    }

    addLength() {
        this.length++;
    }

    occupiesPosition(vector) {
        for(let i = 0; i < this.snakePositions.length; i++) {
            if(this.snakePositions[i].equals(vector)) {
                return true;
            }
        }
        return false;
    }

    kill() {
        this.dead = true;
    }

    update(gameState) {
        super.update(gameState);

        //Check if the current position is a position already occupied by the tail
        //If so, kill the snake
        if(this.occupiesPosition(this.position)) {
            this.kill();
        }

        //Add current position to snake positions array
        this.snakePositions.unshift(new Vector(this.position));
        while(this.snakePositions.length > this.length) {
            this.snakePositions.pop();
        }
    }

    draw(context, gameState) {
        for(let i = 0; i < this.snakePositions.length; i++) {
            let transform = new Vector(this.snakePositions[i])
                .add(gameState.position)
                .multiply(gameState.scale);

            context.fillRect(transform.x, transform.y, 1 * gameState.scale.x, 1 * gameState.scale.y);
        }
    }

    /**
     * Update the snake object using an event listener rather than the Input module.
     * This is done because the game update function runs at a low FPS, so inputs that
     * should have been handled by the snake were being missed making the game feel laggy.
     * @param snake
     */
    setEventHandlers(snake) {
        document.addEventListener('keydown', function(event) {
            //Check for inputs and update snake direction
            //Don't allow the snake to go in the opposite direction it is currently traveling in
            switch(event.keyCode) {
                //Left arrow
                case 37:
                    if(snake.currentDirection !== 'right') {
                        snake.setDirection('left');
                    }
                    break;

                //Up arrow
                case 38:
                    if(snake.currentDirection !== 'down') {
                        snake.setDirection('up');
                    }
                    break;

                //Right arrow
                case 39:
                    if(snake.currentDirection !== 'left') {
                        snake.setDirection('right');
                    }
                    break;

                //Down arrow
                case 40:
                    if(snake.currentDirection !== 'up') {
                        snake.setDirection('down');
                    }
                    break;
            }
        });
    }
}