import Vector from "../Vector";

export default class Base {
    constructor(canvas) {
        this.position = new Vector();

        this.width = canvas.width;
        this.height = canvas.height;

        this.scale = new Vector(1, 1);
    }

    activate(app) {}
    update(app) {}
    draw(canvas, context) {}
}