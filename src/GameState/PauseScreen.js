import Base from "./Base";
import GamePlay from "./GamePlay";
import Text from "../Text";

export default class PauseScreen extends Base {
    allowUnpause;

    textColor = "#ddddee";
    pauseMessage = new Text("Paused", "small-caps bold 52px 'Segoe UI', Helvetica, sans-serif", this.textColor);

    activate(app) {
        this.allowUnpause = false;
    }

    update(app) {
        //Only allow unpausing the game once we have detected that the pause key has been released.
        //This stops the game from rapidly pausing and unpausing
        if(app.input.isKeyUp(80)) { //P key
            this.allowUnpause = true;
        }

        if(app.input.isKeyDown(80) && this.allowUnpause) { //P key
            app.changeGameState(GamePlay.name);
        }
    }

    draw(canvas, context) {
        let textSize = this.pauseMessage.measureText(context);
        this.pauseMessage.drawFilled(context, (this.width - textSize.width) / 2, (this.height / 2));
    }
}