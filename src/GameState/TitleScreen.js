import Base from "./Base";
import GamePlay from "./GamePlay";
import Text from "../Text";

export default class TitleScreen extends Base {
    backgroundColor = "#333333";
    textColor = "#ddddee";

    gameName = new Text("Snake", "small-caps bold 52px 'Segoe UI', Helvetica, sans-serif", this.textColor);
    gameMessage = new Text("Press SPACE to start", "32px 'Segoe UI', Helvetica, sans-serif", this.textColor);

    update(app) {
        if(app.input.isKeyDown(32)) { //Space bar
            let gamePlay = new GamePlay(app.canvas);
            gamePlay.startGame();

            app.changeGameState(gamePlay);
        }
    }

    draw(canvas, context) {
        //Clear canvas
        context.fillStyle = this.backgroundColor;
        context.fillRect(this.position.x, this.position.y, this.width, this.height);

        //Draw title screen text
        let textSize = this.gameName.measureText(context);
        this.gameName.drawFilled(context, (this.width - textSize.width) / 2, (this.height / 2) - 26);

        textSize = this.gameMessage.measureText(context);
        this.gameMessage.drawFilled(context, (this.width - textSize.width) / 2, (this.height / 2) + 26);
    }
}