import Base from "./Base";
import EndScreen from "./EndScreen";
import Fruit from "../Entity/Fruit";
import PauseScreen from "./PauseScreen";
import Snake from "../Entity/Snake";
import Vector from "../Vector";

const CELLS_PER_COLUMN = 25;
const CELLS_PER_ROW = 25;

export default class GamePlay extends Base {
    snakeWinLength = CELLS_PER_COLUMN * CELLS_PER_ROW;

    allowPause;

    score;

    snake;
    snakeColor;

    fruit;
    fruitColor;

    availableColors = [
        '#0e00ff',
        '#00c1ff',
        '#67ff00',
        '#fffa00',
        '#ffac00',
        '#ff0300',
        '#ff00b6',
        '#b600ff'
    ];

    backgroundColor = '#333333';

    constructor(canvas) {
        super(canvas);

        this.scale = new Vector(this.width / CELLS_PER_COLUMN, this.height / CELLS_PER_ROW);
    }

    startGame() {
        this.score = 0;

        this.snake = new Snake(Math.floor(CELLS_PER_COLUMN / 2), Math.floor(CELLS_PER_ROW / 2));
        this.snakeColor = this.getRandomColor();

        this.fruit = new Fruit();
        this.resetFruitPosition(this.fruit);

        //Pick fruit color. Ensure it is not the same as snake color
        do {
            this.fruitColor = this.getRandomColor();
        }
        while (this.fruitColor === this.snakeColor);
    }

    activate(app) {
        this.allowPause = false;
    }

    update(app) {
        this.snake.update(this);
        this.fruit.update(this);

        //Check if the snake has collided with the fruit
        if (this.snake.position.equals(this.fruit.position)) {
            this.collectFruit();
        }

        //Check if the snake has gone off screen
        if (this.snake.position.x < 0
            || this.snake.position.x > CELLS_PER_COLUMN -1
            || this.snake.position.y < 0
            || this.snake.position.y > CELLS_PER_ROW -1
        ) {
            this.snake.kill();
        }

        //If the snake has died, restart the game
        if (this.snake.dead) {
            let endScreen = new EndScreen(app.canvas);
            endScreen.setScore(false, this.score);

            app.changeGameState(endScreen);
            return;
        }

        //If the snake has reached the maximum length, the player has won the game
        if (this.snake.length >= this.snakeWinLength) {
            let endScreen = new EndScreen(app.canvas);
            endScreen.setScore(true, this.score);

            app.changeGameState(endScreen);
            return;
        }

        //Only allow pausing the game once we have detected that the pause key has been released.
        //This stops the game from rapidly pausing and pausing
        if (app.input.isKeyUp(80)) { //P key
            this.allowPause = true;
        }

        //Allow player to pause the game
        if (app.input.isKeyDown(80) && this.allowPause) { //P key
            app.changeGameState(new PauseScreen(app.canvas));
        }
    }

    draw(canvas, context) {
        //Clear canvas
        context.fillStyle = this.backgroundColor;
        context.fillRect(this.position.x, this.position.y, this.width, this.height);

        //Draw snake and fruit
        context.fillStyle = this.fruitColor;
        this.fruit.draw(context, this);

        context.fillStyle = this.snakeColor;
        this.snake.draw(context, this);
    }

    getRandomColor() {
        return this.availableColors[Math.floor(Math.random() * this.availableColors.length)];
    }

    collectFruit() {
        this.score++;
        this.snake.addLength();
        this.resetFruitPosition(this.fruit);
    }

    /**
     * Sets the fruit to be at a random position on the screen, whilst
     * making sure it is not a position occupied by the snake.
     */
    resetFruitPosition(fruit) {
        //Ensure snake is not at the maximum length, or else we will cause an infinite loop
        if(this.snake.length >= this.snakeWinLength) {
            return;
        }

        do {
            fruit.position = new Vector(Math.floor(Math.random() * CELLS_PER_COLUMN), Math.floor(Math.random() * CELLS_PER_ROW));
        }
        while (this.snake.occupiesPosition(fruit.position));
    }
}