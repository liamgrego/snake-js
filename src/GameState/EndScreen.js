import Base from "./Base";
import GamePlay from "./GamePlay";
import Text from "../Text";

export default class EndScreen extends Base {
    backgroundColor = "#333333";
    textColor = "#ddddee";

    winMessage = new Text("", "small-caps bold 52px 'Segoe UI', Helvetica, sans-serif", this.textColor);
    endMessage = new Text("", "small-caps bold 38px 'Segoe UI', Helvetica, sans-serif", this.textColor);
    restartMessage = new Text("Press SPACE to restart", "32px 'Segoe UI', Helvetica, sans-serif", this.textColor);

    setScore(wonGame, score) {
        if(wonGame) {
            this.winMessage.message = "You Won?";
            this.endMessage.message = "No One Can Beat Snake!";
        } else {
            this.winMessage.message = "You Lost!";
            this.endMessage.message = "Your Score Was " + score;
        }
    }

    update(app) {
        if(app.input.isKeyDown(32)) { //Space bar
            let gamePlay = new GamePlay(app.canvas);
            gamePlay.startGame();

            app.changeGameState(gamePlay);
        }
    }

    draw(canvas, context) {
        //Clear canvas
        context.fillStyle = this.backgroundColor;
        context.fillRect(this.position.x, this.position.y, this.width, this.height);

        //Draw end screen text
        let textSize = this.winMessage.measureText(context);
        this.winMessage.drawFilled(context, (this.width - textSize.width) / 2, (this.height / 2) - 64);

        textSize = this.endMessage.measureText(context);
        this.endMessage.drawFilled(context, (this.width - textSize.width) / 2, (this.height / 2) - 10);

        textSize = this.restartMessage.measureText(context);
        this.restartMessage.drawFilled(context, (this.width - textSize.width) / 2, (this.height / 2) + 38);
    };
}