import Input from "./Input";
import PauseScreen from "./GameState/PauseScreen";
import TitleScreen from "./GameState/TitleScreen";
import GamePlay from "./GameState/GamePlay";

const FPS = 16;

export default class App {
    input;
    canvas;
    context;
    activeGameState;
    gameStates;

    constructor(canvas) {
        this.input = new Input();
        this.canvas = canvas;
        this.context = this.canvas.getContext("2d");

        this.activeGameState = null;
        this.gameStates = {};
    }

    run() {
        this.input.initialiseEventListeners();

        this.changeGameState(new TitleScreen(this.canvas));

        //Start game loop
        let _this = this;
        (function gameLoop() {
            requestAnimationFrame(function() {
                _this.update();
                _this.draw();
            });
            setTimeout(gameLoop, 1000 / FPS);
        })();
    }

    /**
     * State can either be an object inheriting from GameState/Base
     * or a string, which should be the constructor of a GameState/Base.
     * If object is passed, this object will be set as the active game state, and
     * any existing GameState of the same type will be overridden.
     * If string is passed, then the last GameState of this type will be set as active.
     * @param state
     */
    changeGameState(state) {
        //String passed, switching to existing GameState of that type
        if(typeof state === "string") {
            if (!this.gameStates[state]) {
                throw new Error('No GameState of ' + state + ' found in current GameStates');
            }

        }
        //Object passed, add to game state array
        else {
            this.gameStates[state.constructor.name] = state;
            state = state.constructor.name;
        }

        this.activeGameState = this.gameStates[state];
        this.activeGameState.activate(this);
    }

    update() {
        if (this.activeGameState) {
            this.activeGameState.update(this);
        }
    }

    draw() {
        if (!this.activeGameState) {
            return;
        }

        //Render GamePlay screen behind the PauseScreen if PauseScreen is active
        if (this.activeGameState instanceof PauseScreen) {
            if(this.gameStates[GamePlay.constructor.name]) {
                this.gameStates[GamePlay.constructor.name].draw(this.canvas, this.context);
            }
        }

        //Render current active game state
        this.activeGameState.draw(this.canvas, this.context);
    }
}