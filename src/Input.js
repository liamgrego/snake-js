export default class Input {
    keysDown = {};

    initialiseEventListeners() {
        document.addEventListener('keydown', (event) => {
            //Add pressed key to array of keys currently pressed
            this.keysDown[event.keyCode] = true;

            //Block page scrolling whilst user is playing the game
            switch(event.keyCode){
                //Arrow keys
                case 37:
                case 39:
                case 38:
                case 40:

                case 32: // Space
                    event.preventDefault();
                    console.log("here")

                    break;
            }
        });

        document.addEventListener('keyup', (event) => {
            this.keysDown[event.keyCode] = false;
        });
    }

    isKeyDown(keyCode) {
        return !!this.keysDown[keyCode];
    }

    isKeyUp(keyCode) {
        return !this.keysDown[keyCode];
    }
}