export default class Text {
    constructor(message, style, color) {
        this.message = message || "";
        this.style = style || null;
        this.color = color || null;
    }

    measureText(context) {
        if(this.style) {
            context.font = this.style;
        }
        return context.measureText(this.message);
    }

    drawFilled(context, x, y) {
        if(this.style) {
            context.font = this.style;
        }
        if(this.color) {
            context.fillStyle = this.color;
        }
        context.fillText(this.message, x, y);
    }
}