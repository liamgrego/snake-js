export default class Vector {
    constructor(x, y) {
        //Initialise from another Vector
        if(x instanceof Vector) {
            this.x = x.x;
            this.y = x.y;
            return;
        }

        //Initialise x and y values
        this.x = x || 0;
        this.y = y || 0;
    }

    static add(vector1, vector2) {
        return new Vector(vector1.x + vector2.x, vector1.y + vector2.y);
    }

    static subtract(vector1, vector2) {
        return new Vector(vector1.x - vector2.x, vector1.y - vector2.y);
    };

    add(vector) {
        this.x += vector.x;
        this.y += vector.y;
        return this;
    }

    subtract(vector) {
        this.x -= vector.x;
        this.y -= vector.y;
        return this;
    }

    multiply(vector) {
        //Multiply by scalar number
        if(typeof vector === "number") {
            this.x *= vector;
            this.y *= vector;
            return this;
        }

        //Multiply by another Vector
        this.x *= vector.x;
        this.y *= vector.y;
        return this;
    }

    divide(vector) {
        //Divide by scalar number
        if(typeof vector === "number") {
            this.x /= vector;
            this.y /= vector;
            return this;
        }

        //Divide by another Vector
        this.x /= vector.x;
        this.y /= vector.y;
        return this;
    }

    equals(vector) {
        return (
            this.x === vector.x
            && this.y === vector.y
        );
    }
}