Snake game made in HTML 5 and JavaScript.

To build first run: npm install
Once this is done, run: npm run build

This will create the compiled game in: dist/main.js

Then you can open: index.html to play the game.